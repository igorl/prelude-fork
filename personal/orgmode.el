;; org-mode stuff
(global-set-key (kbd "C-c o")
                (lambda () (interactive) (find-file "~/Documents/cs/org/outfit7.org")))
(global-set-key (kbd "C-c b")
                (lambda () (interactive) (find-file "~/Documents/cs/org/private.org")))
(global-set-key (kbd "C-c l") 'org-store-link)
(global-set-key (kbd "C-c a") 'org-agenda)
(global-set-key (kbd "C-c c") 'org-capture)
(setq org-log-into-drawer t)
(setq org-default-notes-file "~/Documents/cs/org/outfit7.org")
(setq org-todo-keywords
      '((sequence "TODO" "FOLLOWUP" "DONE")))
(setq org-agenda-files (directory-files-recursively "~/Documents/cs/org/" "\\.org$"))
